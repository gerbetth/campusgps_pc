package org.mapsforge.core.os;

public final class SystemClock {
	/**
	 * This class is uninstantiable.
	 */

	private static long sBootTime = System.currentTimeMillis();

	private SystemClock() {
		// This space intentionally left blank.
	}

	/**
	 * Waits a given number of milliseconds (of uptimeMillis) before returning.
	 * Similar to {@link java.lang.Thread#sleep(long)}, but does not throw
	 * {@link InterruptedException}; {@link Thread#interrupt()} events are
	 * deferred until the next interruptible operation. Does not return until at
	 * least the specified number of milliseconds has elapsed.
	 * 
	 * @param ms
	 *            to sleep before returning, in milliseconds of uptime.
	 */
	public static void sleep(long ms) {
		long start = uptimeMillis();
		long duration = ms;
		boolean interrupted = false;
		do {
			try {
				Thread.sleep(duration);
			} catch (InterruptedException e) {
				interrupted = true;
			}
			duration = start + ms - uptimeMillis();
		} while (duration > 0);

		if (interrupted) {
			// Important: we don't want to quietly eat an interrupt() event,
			// so we make sure to re-interrupt the thread so that the next
			// call to Thread.sleep() or Object.wait() will be interrupted.
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * Sets the current wall time, in milliseconds. Requires the calling process
	 * to have appropriate permissions.
	 * 
	 * @return if the clock was successfully set to the specified time.
	 */
	// native public static boolean setCurrentTimeMillis(long millis);

	/**
	 * Returns milliseconds since boot, not counting time spent in deep sleep.
	 * <b>Note:</b> This value may get reset occasionally (before it would
	 * otherwise wrap around).
	 * 
	 * @return milliseconds of non-sleep uptime since boot.
	 */
	// native public static long uptimeMillis();
	public static long uptimeMillis() {
		return System.currentTimeMillis() - sBootTime;
	}
	/**
	 * Returns milliseconds since boot, including time spent in sleep.
	 * 
	 * @return elapsed milliseconds since boot.
	 */
	// native public static long elapsedRealtime();

	/**
	 * Returns milliseconds running in the current thread.
	 * 
	 * @return elapsed milliseconds in the thread
	 */
	// public static native long currentThreadTimeMillis();
}